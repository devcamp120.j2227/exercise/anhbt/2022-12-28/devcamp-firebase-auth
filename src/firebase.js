// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getAuth } from "firebase/auth";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBa1IRZGfQygenrcfrvKo83FwWmWTR3gfw",
  authDomain: "devcamp-buitienanh.firebaseapp.com",
  projectId: "devcamp-buitienanh",
  storageBucket: "devcamp-buitienanh.appspot.com",
  messagingSenderId: "439840356228",
  appId: "1:439840356228:web:1201b9e225b87da10d4fd1"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export default auth;